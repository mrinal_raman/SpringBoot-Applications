package com.SpringBootPractice.SpringBootPractice.dao;

import java.util.List;

import com.SpringBootPractice.SpringBootPractice.entity.Employee;

public interface EmployeeDAO {

	public List<Employee> getAllEmployees();

	public Employee findEmployeeById(int id);

	public void saveEmployee(Employee empl);

	public void deleteEmployeeById(int id);
}
