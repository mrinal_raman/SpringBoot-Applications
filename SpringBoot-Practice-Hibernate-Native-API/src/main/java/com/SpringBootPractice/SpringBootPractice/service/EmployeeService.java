package com.SpringBootPractice.SpringBootPractice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.SpringBootPractice.SpringBootPractice.dao.EmployeeDAOImpl;
import com.SpringBootPractice.SpringBootPractice.entity.Employee;

@Service
@Transactional
public class EmployeeService {

	@Autowired
	private EmployeeDAOImpl daoImpl;

	public List<Employee> getAllEmployees() {
		return daoImpl.getAllEmployees();
	}

	public Employee findEmployeeById(int id) {
		return daoImpl.findEmployeeById(id);
	}

	public void saveEmployee(Employee empl) {
		daoImpl.saveEmployee(empl);
	}

	public void deleteEmployeeById(int id) {
		daoImpl.deleteEmployeeById(id);

	}
}
