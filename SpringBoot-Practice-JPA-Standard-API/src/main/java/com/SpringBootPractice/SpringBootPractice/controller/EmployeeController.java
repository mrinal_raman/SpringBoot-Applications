package com.SpringBootPractice.SpringBootPractice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.SpringBootPractice.SpringBootPractice.entity.Employee;
import com.SpringBootPractice.SpringBootPractice.service.EmployeeService;

@RestController
public class EmployeeController {
	@Autowired
	private EmployeeService service;

	@GetMapping("/getAllEmployees")
	public List<Employee> getEmployeeList() {
		return service.getAllEmployees();
	}

	@GetMapping("/getEmployeeById/{id}")
	public Employee findEmployeeById(@PathVariable int id) {
		Employee empl = service.findEmployeeById(id);
		if (empl == null)
			throw new RuntimeException("Employee is not found for this id");
		else
			return empl;
	}

	@PostMapping("/addNewEmployee")
	public void saveEmployee(@RequestBody Employee empl) {
		service.saveEmployee(empl);
	}

	@DeleteMapping("/deleteById/{id}")
	public String deleteById(@PathVariable int id) {
		Employee tempEmployee = service.findEmployeeById(id);
		// null check:
		if (tempEmployee == null)
			throw new RuntimeException("Employee doesn't exist...");
		else
			service.deleteEmployeeById(id);

		return "done";
	}

	// update employee- PutMapping:

	@PutMapping("/updateEmployee")
	public Employee updateEmployee(@RequestBody Employee empl) {
		service.saveEmployee(empl);
		return empl;
	}

}
