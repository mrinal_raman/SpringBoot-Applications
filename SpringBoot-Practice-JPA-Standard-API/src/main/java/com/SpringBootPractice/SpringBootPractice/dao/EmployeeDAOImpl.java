package com.SpringBootPractice.SpringBootPractice.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.SpringBootPractice.SpringBootPractice.entity.Employee;

// using JPA DAO implementatiob:
@Repository
public class EmployeeDAOImpl implements EmployeeDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Employee> getAllEmployees() {

		// create the hibernate session:
		// Session currentSession = entityManager.unwrap(Session.class);

		// create a query:
		Query query1 = entityManager.createQuery("from Employee");

		// execute the query and get the resut:
		List<Employee> list = query1.getResultList();

		return list;

	}

	@Override
	public Employee findEmployeeById(int id) {

		// get the current hibernate session
		// Session currentSession = entityManager.unwrap(Session.class);

		// get the employee
		Employee empl = entityManager.find(Employee.class, id);

		// return the employee
		return empl;
	}

	@Override
	public void saveEmployee(Employee empl) {
		// get the current hibernate session
		// Session currentSession = entityManager.unwrap(Session.class);

		// save employee

		Employee dbEmployee = entityManager.merge(empl);
		empl.setId(dbEmployee.getId());
	}

	@Override
	public void deleteEmployeeById(int id) {
		// get the current hibernate session:
		// Session currentSession = entityManager.unwrap(Session.class);

		// delete the employee by primary key id:
		Query query = entityManager.createQuery("delete from Employee where id=:id");
		query.setParameter("id", id);
		query.executeUpdate();
	}

}
