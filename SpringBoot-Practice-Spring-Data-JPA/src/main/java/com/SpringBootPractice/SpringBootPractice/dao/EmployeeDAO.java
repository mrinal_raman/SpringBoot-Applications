package com.SpringBootPractice.SpringBootPractice.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SpringBootPractice.SpringBootPractice.entity.Employee;

public interface EmployeeDAO extends JpaRepository<Employee, Integer> {

}
