package com.SpringBootPractice.SpringBootPractice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.SpringBootPractice.SpringBootPractice.dao.EmployeeDAO;
import com.SpringBootPractice.SpringBootPractice.entity.Employee;

@Service
@Transactional
public class EmployeeService {

	@Autowired
	private EmployeeDAO dao;

	public List<Employee> getAllEmployees() {
		return dao.findAll();
	}

	public Optional<Employee> findEmployeeById(int id) {
		return dao.findById(id);
	}

	public void saveEmployee(Employee empl) {
		dao.save(empl);
	}

	public void deleteEmployeeById(int id) {
		dao.deleteById(id);

	}
}
